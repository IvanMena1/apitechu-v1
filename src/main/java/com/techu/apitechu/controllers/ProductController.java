package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es:" + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                result = product;
                break;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel product) {
        System.out.println("createProduct");
        System.out.println("id del nuevo producto es:" + product.getId());
        System.out.println("descripción del nuevo producto es:" + product.getDesc());
        System.out.println("precio del nuevo producto es:" + product.getPrice());

        ApitechuApplication.productModels.add(product);

        return product;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel updateProduct){
        System.out.println("updateProduct");
        System.out.println("id del producto a actualizar es:" + id);
        System.out.println("id nuevo es:" + updateProduct.getId());
        System.out.println("descripción nueva es:" + updateProduct.getDesc());
        System.out.println("precio nuevo es:" + updateProduct.getPrice());

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                product.setId(updateProduct.getId());
                product.setPrice(updateProduct.getPrice());
                product.setDesc(updateProduct.getDesc());
                break;
            }
        }

        return updateProduct;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("id del producto a eliminar es:" + id);

        ProductModel deleteProduct = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                foundProduct = true;
                deleteProduct = product;
                break;
            }
        }

        if (foundProduct) {
            ApitechuApplication.productModels.remove(deleteProduct);
        }

        return deleteProduct;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updatePartialProduct(@PathVariable String id, @RequestBody ProductModel updateProduct){
        System.out.println("updatePartialProduct");
        System.out.println("id del producto a actualizar es:" + id);
        System.out.println("descripción nueva es:" + updateProduct.getDesc());
        System.out.println("precio nuevo es:" + updateProduct.getPrice());

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                updateProduct.setId(id);
                if (updateProduct.getPrice() > 0.0){
                    product.setPrice(updateProduct.getPrice());
                } else {
                    updateProduct.setPrice(product.getPrice());
                }
                if (updateProduct.getDesc() != null){
                    product.setDesc(updateProduct.getDesc());
                } else {
                    updateProduct.setDesc(product.getDesc());
                }
                break;
            }
        }

        return updateProduct;
    }
}
